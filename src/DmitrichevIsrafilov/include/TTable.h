#ifndef INCLUDE_TTABLE_H_
#define INCLUDE_TTABLE_H_

#include <string>

#include "TDataCom.h"
#include "TDatValue.h"
#include "TTabRecord.h"

class TTable : public TDataCom
{
protected:
	int DataCount;		// ����� ������� � �������
	int Efficiency;		// ���������� ������������� ���������� ��������

public:
	TTable() { DataCount = 0; Efficiency = 0; }
	virtual ~TTable() { };

	// �������������� ������
	bool IsEmpty() const { return DataCount == 0; }
	virtual bool IsFull() const = 0;
	int GetDataCount() const { return DataCount; }
	int GetEfficiency() const { return Efficiency; }

	// ������
	virtual TKey GetKey() const = 0;
	virtual PTDatValue GetValuePtr() const = 0;

	// ���������
	virtual void Reset() = 0;
	virtual bool IsTabEnded() const = 0;
	virtual Data GoNext() = 0;

	// �������� ������
	virtual PTDatValue FindRecord(TKey k) = 0;
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0;
	virtual void DelRecord(TKey k) = 0;
};

#endif
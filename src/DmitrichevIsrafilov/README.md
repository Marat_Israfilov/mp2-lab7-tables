## Отчет по лабораторной работе №7

### тема "Таблицы"

## Введение:

Представление данных во многих задачах из разных областей человеческой деятельности может быть организовано при помощи таблиц. Таблицы представляют собой последовательности строк (записей), структура строк может быть различной, но обязательным является поле, задающее имя (ключ) записи. Таблицы применяются в бухгалтерском учете (ведомости заработной платы), в торговле (прайс-листы), в образовательных учреждениях (экзаменационные ведомости) и являются одними из наиболее распространенных структур данных, используемых при создании системного и прикладного математического обеспечения. Таблицы широко применяются в трансляторах (таблицы идентификаторов) и операционных системах, могут рассматриваться как программная реализация ассоциативной памяти и т.п. Существование отношения «иметь имя» является обязательным в большинстве разрабатываемых программистами структур данных; доступ по имени в этих структурах служит для получения соответствия между адресным принципом указания элементов памяти ЭВМ и общепринятым (более удобным для человека) способом указания объектов по их именам.

Целью лабораторной работы помимо изучения способов организации таблиц является начальное знакомство с принципами проектирования структур хранения, используемых в методах решения прикладных задач. На примере таблиц изучаются возможность выбора разных вариантов структур хранения, анализ их эффективности и определения областей приложений, в которых выбираемые структуры хранения являются наиболее эффективными.

В качестве практической задачи, на примере которой будут продемонстрированы возможные способы организации таблиц, рассматривается проблема статистической обработки результатов экзаменационной успеваемости студентов (выполнение таких, например, вычислений как определение среднего балла по предмету и/или по группе при назначении студентов на стипендию или при распределении студентов по кафедрам и т.п.).

##  Основные понятия и определения

**Таблица** (от лат. tabula – доска) – динамическая структура данных, базисным множеством которой является семейство линейных структур из записей (базисное отношение включения определяется операциями вставки и удаления записей).

**Запись** – кортеж, каждый элемент которого обычно именуется полем.

**Имя записи** (ключ) – одно из полей записи, по которому обычно осуществляется поиск записей в таблице; остальные поля образуют тело записи.

**Двоичное дерево поиска** – это представление данных в виде дерева, для которого выполняются условия:

- для любого узла (вершины) дерева существует не более двух потомков (двоичное дерево);
- для любого узла значения во всех узлах левого поддерева меньше значения в узле;
- для любого узла значения во всех узлах правого поддерева больше значения в узле.

## Требования к лабораторной работе

В рамках данной лабораторной работы ставится задача создания программных средств, поддерживающих табличные динамические структуры данных (таблицы) и базовые операции над ними:

-	поиск записи;
-	вставка записи (без дублирования);
-	удаление записи.

Выполнение операций над таблицами может осуществляться с различной степенью эффективности в зависимости от способа организации таблицы. 

В рамках лабораторной работы как показатель эффективности предлагается использовать количество операций, необходимых для выполнения операции поиска записи в таблице. Величина этого показателя должна определяться как аналитически (при использовании тех или иных упрощающих предположений), так и экспериментально на основе проведения вычислительных экспериментов.

В лабораторной работе предлагается реализовать следующие типы таблиц:

-	просмотровые (неупорядоченные);
-	упорядоченные (сортированные);
-	таблицы со структурами хранения на основе деревьев поиска;

## Структура проекта

При выполнении данной лабораторной работы следует разработать иерархию классов, учитывая, что все таблицы имеют как общие свойства (их описание следует поместить в определении базового класса), так и особенности выполнения отдельных операций (реализуются в отдельных классах для каждого вида таблиц). При разработке классов используется ранее разработанный класс **TDatValue**.
 
Рекомендуемый состав классов приведен ниже.

**TTabRecord.h**, **TTabRecord.cpp** – модуль с классом объектов-значений для записей таблицы;

**TTable.h** – абстрактный базовый класс, содержит спецификации методов таблицы;

**TArrayTable.h**, **TArrayTable.cpp** – абстрактный базовый класс для таблиц с непрерывной памятью;

**TScanTable.h**, **TScanTable.cpp** – модуль с классом, обеспечивающим реализацию просматриваемых таблиц;

**TSortTable.h**, **TSortTable.cpp** – модуль с классом, обеспечивающим реализацию упорядоченных таблиц;

**TTreeNode.h**, **TTreeNode.cpp** – модуль с абстрактным базовым классом объектов-значений для деревьев;

**TTreeTable.h**, **TTreeTable.cpp** – модуль с классом, реализующим таблицы в виде деревьев поиска;

**TBalanceNode.h**, **TBalanceNode.cpp** – модуль с базовым классом объектов-значений для сбалансированных деревьев;

**TBalanceTree.h**, **TBalanceTree.cpp** – модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска;

**TTableTestkit.cpp** – модуль программы тестирования.

## Классы

### Класс объектов-значений для записей таблицы (файл TTabRecord.h):

#### Объявление:

```c++
#include <iostream>
#include "TDatValue.h"

typedef std::string TKey;

class TTabRecord : public TDatValue {
protected:
	TKey Key; // Кключ записи
	PTDatValue pValue; // указатель на значение

public:
	TTabRecord(TKey k = "", PTDatValue pVal = NULL);
	TTabRecord(const TTabRecord&) = delete;
	void SetKey(TKey k);
	TKey GetKey() const;
	void SetValuePtr(PTDatValue p);
	PTDatValue GetValuePtr() const;
	virtual TDatValue* GetCopy();
	TTabRecord& operator=(TTabRecord& tr);
	virtual bool operator==(const TTabRecord& tr);
	virtual bool operator<(const TTabRecord& tr);
	virtual bool operator>(const TTabRecord& tr);

	friend class TArrayTable;
	friend class TScanTable;
	friend class TSortTable;
	friend class TTreeNode;
};

typedef TTabRecord* PTTabRecord;
```


#### Определение:

```c++
#include "TTabRecord.h"

TTabRecord::TTabRecord(TKey k, PTDatValue pVal)
{
	Key = k;
	pValue = pVal;
}

void TTabRecord::SetKey(TKey k) {
	Key = k;
}

TKey TTabRecord::GetKey() const {
	return Key;
}

void TTabRecord::SetValuePtr(PTDatValue p) {
	pValue = p;
}

PTDatValue TTabRecord::GetValuePtr() const {
	return pValue;
}

TDatValue* TTabRecord::GetCopy()
{
	TDatValue* temp = new TTabRecord(Key, pValue);
	return temp;
}

TTabRecord& TTabRecord::operator=(TTabRecord& tr)
{
	Key = tr.Key;
	pValue = tr.pValue;
	return *this;
}

bool TTabRecord::operator==(const TTabRecord& tr)
{
	return Key == tr.Key;
}

bool TTabRecord::operator<(const TTabRecord& tr)
{
	return Key < tr.Key;
}

bool TTabRecord::operator>(const TTabRecord& tr)
{
	return Key > tr.Key;
}
```

Тип TKey описывает тип значений ключей записи. В данном примере для типа ключей использован класс string из стандартной библиотеки шаблонов (STL). 

### Абстрактный базовый класс содержит спецификации методов таблицы (TTable.h).

#### Объявление:

```c++
#include <string>

#include "TDataCom.h"
#include "TDatValue.h"
#include "TTabRecord.h"

class TTable : public TDataCom
{
protected:
	int DataCount;		// число записей в таблице
	int Efficiency;		// показатель эффективности выполнени¤ операции

public:
	TTable() { DataCount = 0; Efficiency = 0; }
	virtual ~TTable() { };

	// информационные методы
	bool IsEmpty() const { return DataCount == 0; }
	virtual bool IsFull() const = 0;
	int GetDataCount() const { return DataCount; }
	int GetEfficiency() const { return Efficiency; }

	// доступ
	virtual TKey GetKey() const = 0;
	virtual PTDatValue GetValuePtr() const = 0;

	// навигаци¤
	virtual void Reset() = 0;
	virtual bool IsTabEnded() const = 0;
	virtual Data GoNext() = 0;

	// ќсновные методы
	virtual PTDatValue FindRecord(TKey k) = 0;
	virtual void InsRecord(TKey k, PTDatValue pVal) = 0;
	virtual void DelRecord(TKey k) = 0;
};
```

### класс для таблиц с непрерывной памятью служит для управления структурой хранения (TArrayTable)

#### Объявление:

```c++
#include "TTable.h"
#include "TTabRecord.h"

enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class TArrayTable : public TTable
{
protected:
	PTTabRecord *pRecs;				// Память для записей таблицы
	int TabSize;					// макс. число записей в таблице
	int CurrPos;					// номер текущей записи

public:
	static const int TAB_MAX_SIZE = 25;
	TArrayTable(int size = TAB_MAX_SIZE);
	~TArrayTable();
	
	// информационные методы
	virtual bool IsFull() const { return DataCount >= TabSize; }
	int GetTabSize() const { return TabSize; }

	// доступ
	virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
	virtual PTDatValue GetValuePtr() const { return GetValuePtr(CURRENT_POS); }
	virtual TKey GetKey(TDataPos mode) const;
	virtual PTDatValue GetValuePtr(TDataPos mode) const;

// для тестов
	PTTabRecord GetCurrRecord();

	// навигация
	virtual void Reset();
	virtual bool IsTabEnded() const;
	virtual Data GoNext();
	virtual Data SetCurrentPos(int pos);
	int GetCurrentPos() const { return CurrPos; }
	
	friend TSortTable;
};
```

#### Определение:

```c++
#include "TArrayTable.h"

TArrayTable::TArrayTable(int size) {
	pRecs = new PTTabRecord[size];
	for (int i = 0; i < size; ++i) pRecs[i] = nullptr;
	TabSize = size;
	DataCount = CurrPos = 0;
}

TArrayTable::~TArrayTable()
{
	for (int i = 0; i < DataCount; ++i)
		delete pRecs[i];
	delete[] pRecs;
}

TKey TArrayTable::GetKey(TDataPos mode) const
{
	int pos = -1;
	switch (mode)
	{
	case TDataPos::FIRST_POS:
		pos = 0;
		break;
	case TDataPos::LAST_POS:
		pos = DataCount - 1;
		break;
	case TDataPos::CURRENT_POS:
		pos = CurrPos;
		break;
	}
	return (pos != -1) ? pRecs[pos]->Key : "";
}


PTDatValue TArrayTable::GetValuePtr(TDataPos mode) const
{
	int pos = -1;
	switch (mode)
	{
	case TDataPos::FIRST_POS:
		pos = 0;
		break;
	case TDataPos::LAST_POS:
		pos = DataCount - 1;
		break;
	case TDataPos::CURRENT_POS:
		pos = CurrPos;
		break;
	}
	return (pos != -1) ? pRecs[pos]->pValue : nullptr;
}

PTTabRecord TArrayTable::GetCurrRecord() {
	return pRecs[CurrPos];
}


void TArrayTable::Reset() {
	CurrPos = 0;
}

bool TArrayTable::IsTabEnded() const
{
	return CurrPos >= DataCount;
}

Data TArrayTable::GoNext()
{
	if (!IsTabEnded())
		CurrPos++;
	else
		SetRetCode(Data::INCORRECT_INCOMING);
	return GetRetCode();
}

Data TArrayTable::SetCurrentPos(int pos)
{
	if (pos < 0 || pos >= DataCount)
		SetRetCode(Data::INCORRECT_INCOMING);
	else
		CurrPos = pos;
	return GetRetCode();
}
```
Данный класс обеспечивает управление памятью (выделение и освобождение памяти). Методы доступа применимы к первой, текущей и последней записям таблицы, желаемый вариант доступа задается через параметр метода доступа. Методы SetCurrentPos (установить текущую позицию на запись с заданным номером) и GetCurrentPos (получить номер текущей записи) вводят операции прямого доступа к записям таблицы. В классе реализованы методы навигации.

### Класс, обеспечивающий реализацию просматриваемых таблиц (TScanTable).

#### Объявление:

```c++
#include "TArrayTable.h"

class TScanTable : public TArrayTable
{
public:
	TScanTable(int Size) : TArrayTable(Size) { }

	// ќсновные методы
	virtual PTDatValue FindRecord(TKey k);
	virtual void InsRecord(TKey k, PTDatValue pVal);
	virtual void DelRecord(TKey k);
};
```

#### Определение:

```c++
#include "TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
	SetRetCode(Data::OK);

	for (int i = 0; i < TabSize; ++i) {
		if(pRecs[i] != nullptr) 
			if (pRecs[i]->GetKey() == k) {
				return pRecs[i]->GetValuePtr();
			}

	}
	SetRetCode(Data::NO_RECORD);
	return nullptr;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
	SetRetCode(Data::OUT_OF_RANGE);
	for (int i = 0; i < TabSize; ++i) {
		if (pRecs[i] == nullptr)
		{
			pRecs[i] = new TTabRecord(k, pVal);
			DataCount++;
			SetRetCode(Data::OK);
			break;
		}
	}
}

void TScanTable::DelRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	for (int i = 0; i < TabSize; i++) {
		if (pRecs[i] != nullptr)
			if (pRecs[i]->GetKey() == k) {
				delete pRecs[i];
				pRecs[i] = pRecs[DataCount - 1];
				pRecs[DataCount - 1] = nullptr;
				DataCount--;
				SetRetCode(Data::OK);
			}
	}
}
```

### Упорядоченные таблицы (TSortTable).

#### Объявление:

```c++
#include "TScanTable.h"

enum TSortMethod { INSERT_SORT, MERGE_SORT, QUICK_SORT };

class TSortTable : public TScanTable
{
protected:
	TSortMethod SortMethod; // метод сортировки
	void SortData(void);   // сортировка данных
	void InsertSort(PTTabRecord *pMem, int DataCount); // метод вставок
	void MergeSort(PTTabRecord *pMem, int DataCount);  // метод слияния
	void MergeSorter(PTTabRecord * &pData, PTTabRecord * &pBuff, int Size);
	void MergeData(PTTabRecord *&pData, PTTabRecord *&pBuff, int n1, int n2);
	void QuickSort(PTTabRecord *pMem, int DataCount); // быстрая сортировка
	void QuickSplit(PTTabRecord *pData, int Size, int &Pivot);
public:
	TSortTable(int Size) : TScanTable(Size) {};// конструктор
	TSortTable & operator=(const TScanTable &tab); // присваивание
	TSortMethod GetSortMethod(void);    // получить метод сортировки
	void SetSortMethod(TSortMethod sm);// установить метод сортировки
									   // основные методы
	virtual PTDatValue FindRecord(TKey k); // найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal); // вставить
	virtual void DelRecord(TKey k);        // удалить запись
};
```

#### Определение:

```c++
#include "TSortTable.h"

TSortTable & TSortTable::operator=(const TScanTable &tab)
{
	if (pRecs != nullptr) {
		for (int i = 0; i < DataCount; ++i)
			delete pRecs[i];
		delete[] pRecs;
		pRecs = nullptr;
	}
	TabSize = tab.TabSize;
	DataCount = tab.DataCount;
	pRecs = new PTTabRecord[TabSize];
	for (int i = 0; i < DataCount; ++i)
		pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();
	SortData();
	CurrPos = 0;
	return *this;
}

PTDatValue TSortTable::FindRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	PTDatValue result = nullptr;
	if (DataCount > 0) {
		int i, i1 = 0, i2 = DataCount - 1;
		while (i1 <= i2) {
			i = (i1 + i2) / 2;
			if (pRecs[i]->GetKey() == k) {
				result = pRecs[i]->GetValuePtr();
				SetRetCode(Data::OK);
				break;
			}
			else if (pRecs[i]->GetKey() > k) {
				i2 = i - 1;
			}
			else {
				i1 = i + 1;
			}
		}
	}
	return result;
}

void TSortTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(Data::FULL_TAB);
	}
	else {
		if (FindRecord(k) != nullptr) {
			SetRetCode(Data::DOUBLE_REC);
		}
		else {
			SetRetCode(Data::OK);
			for (int i = DataCount; i > CurrPos; i--)
				pRecs[i] = pRecs[i - 1];
			pRecs[CurrPos] = new TTabRecord(k, pVal);
			DataCount++;
			SortData();
		}
	}
}

void TSortTable::DelRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	for (int i = 0; i < DataCount; i++) {
		if (pRecs[i]->GetKey() == k) {
			delete pRecs[i];
			for (int j = i; j < DataCount; j++)
				pRecs[j] = pRecs[j + 1];
			DataCount--;
			SetRetCode(Data::OK);
		}
	}
}

TSortMethod TSortTable::GetSortMethod()
{
	return SortMethod;
}

void TSortTable::SetSortMethod(TSortMethod method)
{
	SortMethod = method;
}


void TSortTable::SortData()
{
	switch (SortMethod) {
	case TSortMethod::QUICK_SORT:
		QuickSort(pRecs, DataCount);
		break;
	case TSortMethod::INSERT_SORT:
		InsertSort(pRecs, DataCount);
		break;
	case TSortMethod::MERGE_SORT:
		QuickSort(pRecs, DataCount);
		break;
	default:
		InsertSort(pRecs, DataCount);
		break;
	}
}

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount)
{
	PTTabRecord pR;
	for (int i = 1, j; i < DataCount; i++)
	{
		pR = pRecs[i];
		for (j = i - 1; j >= 0; j--) {
			if (pRecs[j]->GetKey() > pR->GetKey()) {
				pRecs[j + 1] = pRecs[j];
			}
			else {
				break;
			}
		}
		pRecs[j + 1] = pR;
	}
}


void TSortTable::MergeSort(PTTabRecord * pMem, int DataCount)
{
	PTTabRecord *pData = pRecs;
	PTTabRecord *pBuff = new PTTabRecord[DataCount];
	PTTabRecord *pTemp = pBuff;
	MergeSorter(pData, pBuff, DataCount);
	if (pData == pTemp) {
		for (int i = 0; i < DataCount; i++)
			pBuff[i] = pData[i];
	}
	delete pTemp;
}

void TSortTable::MergeSorter(PTTabRecord *& pData, PTTabRecord *& pBuff, int Size)
{
	int n1 = (Size + 1) / 2;
	int n2 = Size - n1;
	if (Size > 2) {
		PTTabRecord *pDat2 = pData + n1, *pBuff2 = pBuff + n1;
		MergeSorter(pData, pBuff, n1);
		MergeSorter(pDat2, pBuff2, n2);
	}
	MergeData(pData, pBuff, n1, n2);
}

void TSortTable::MergeData(PTTabRecord *& pData, PTTabRecord *& pBuff, int n1, int n2)
{
	for (int i = 0; i < (n1 + n2); i++) {
		pBuff[i] = pData[i];
	}
	PTTabRecord *&tmp = pData;
	pData = pBuff;
	pBuff = tmp;
}

void TSortTable::QuickSort(PTTabRecord * pRecs, int DataCount)
{
	int pivot; // индекс ведущего элемента
	int n1, n2; // размеры разделенных блоков данных
	if (DataCount > 1) {
		QuickSplit(pRecs, DataCount, pivot);
		n1 = pivot + 1;
		n2 = DataCount - n1;
		QuickSort(pRecs, n1 - 1);
		QuickSort(pRecs+n1, n2);
	}
}

void TSortTable::QuickSplit(PTTabRecord * pData, int Size, int &Pivot)
{
	PTTabRecord pPivot = pData[0], pTemp;
	int i1 = 1, i2 = Size - 1;
	while (i1 <= i2) {
		while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey())) i1++;
		while (pData[i2]->GetKey() > pPivot->GetKey()) i2--;
		if (i1 < i2) {
			pTemp = pData[i1];
			pData[i1] = pData[i2];
			pData[i2] = pTemp;
		}
	}
	pData[0] = pData[i2];
	pData[i2] = pPivot;
	Pivot = i2;
}
```

### Абстрактный базовый класс объектов-значений для деревьев (TTreeNode).

#### Объявление:

```c++
#include "TTabRecord.h"

class TTreeNode;
typedef TTreeNode *PTTreeNode;

class TTreeNode : public TTabRecord {
protected:
	PTTreeNode pLeft, pRight;

public:
	TTreeNode(TKey k = "", PTDatValue pVal = nullptr, PTTreeNode pL = nullptr,
		PTTreeNode pR = nullptr) : TTabRecord(k, pVal), pLeft(pL), pRight(pR) {};
	PTTreeNode GetLeft(void) const;  // указатель на левое поддерево
	PTTreeNode GetRight(void) const; // указатель на правое поддерево
	virtual TDatValue * GetCopy() override;  // изготовить копию

	friend class TTreeTable;
	friend class TBalanceTree;
};
```

#### Определение:

```c++
#include "TTreeNode.h"

PTTreeNode TTreeNode::GetLeft(void) const
{
    return pLeft;
}

PTTreeNode TTreeNode::GetRight(void) const
{
    return pRight;
}

TDatValue * TTreeNode::GetCopy()
{
    TTreeNode *tmp = new TTreeNode(Key, pValue, nullptr, nullptr);
    return tmp;
}
```

### Таблицы со структурой хранения в виде деревьев поиска (TTreeTable).

#### Объявление:

```c++
#include <stack>
#include "TTable.h"
#include "TTreeNode.h"

class  TTreeTable : public TTable {
protected:
	PTTreeNode pRoot;			// указатель на корень дерева
	PTTreeNode *ppRef;			// адрес указателя на вершину-результата в FindRecord
	PTTreeNode pCurrent;		// указатель на текущую вершину
	int CurrPos;				// номер текущей вершины
	std::stack < PTTreeNode> St;		// стек для итератора
	void DeleteTreeTab(PTTreeNode pNode); // удаление
public:
	TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = NULL; ppRef = NULL; }
	~TTreeTable() { DeleteTreeTab(pRoot); }				// деструктор
	
	// информационные методы
	virtual bool IsFull() const override;						//заполнена?
	
	//основные методы
	virtual PTDatValue FindRecord(TKey k) override;				// найти запись
	virtual void InsRecord(TKey k, PTDatValue pVal) override; 	// вставить
	virtual void DelRecord(TKey k) override;					// удалить запись
	
	// навигация
	virtual TKey GetKey(void) const override;
	virtual PTDatValue GetValuePtr(void) const override;
	virtual void Reset(void) override;					// установить на первую запись
	virtual bool IsTabEnded(void) const override;		// таблица завершена?
	virtual Data GoNext(void) override;					// переход к следующей записи
};
```

#### Определение:

```c++
#include "TTreeTable.h"

bool TTreeTable::IsFull() const
{
	return false;
}

PTDatValue TTreeTable::FindRecord(TKey k) {
	PTTreeNode tmp = pRoot;
	ppRef = &pRoot;
	while (tmp != nullptr) {
		if (tmp->GetKey() == k) break;
		if (tmp->GetKey() < k) ppRef = &tmp->pRight;
		else ppRef = &tmp->pLeft;
		tmp = *ppRef;
	}
	if (tmp == nullptr) {
		SetRetCode(Data::NO_RECORD);
		return nullptr;
	}
	else {
		SetRetCode(Data::OK);
		return tmp->GetValuePtr();
	}
}

void TTreeTable::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(Data::FULL_TAB);
	}
	else {
		if (FindRecord(k) != nullptr) {
			SetRetCode(Data::DOUBLE_REC);
		}
		else {
			*ppRef = new TTreeNode(k, pVal);
			DataCount++;
		}
	}
}

void TTreeTable::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(Data::NO_RECORD);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}
		// удаление листа
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					else if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (справа)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (слева)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с двумя потомками
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}

	}
}

TKey TTreeTable::GetKey(void) const
{
	return (pCurrent == nullptr) ? "" : pCurrent->GetKey();
}

PTDatValue TTreeTable::GetValuePtr(void) const
{
	return (pCurrent == nullptr) ? nullptr : pCurrent->GetValuePtr();
}

void TTreeTable::DeleteTreeTab(PTTreeNode pNode)
{
	if (pNode != nullptr) {
		DeleteTreeTab(pNode->GetLeft());
		DeleteTreeTab(pNode->GetRight());
		delete pNode;
	}
}

void TTreeTable::Reset(void)
{
	PTTreeNode pNode = pCurrent = pRoot;
	CurrPos = 0;
	while (pNode != nullptr) {
		St.push(pNode);
		pCurrent = pNode;
		pNode = pNode->GetLeft();
	}
	SetRetCode(Data::OK);
}

bool TTreeTable::IsTabEnded(void) const
{
	return (CurrPos >= DataCount);
}

Data TTreeTable::GoNext(void)
{
	CurrPos++;
	if (!IsTabEnded() && (pCurrent != nullptr)) {
		PTTreeNode pNode = pCurrent = pCurrent->GetRight();
		St.pop();
		while (pNode != nullptr) {
			St.push(pNode);
			pCurrent = pNode;
			pNode = pNode->GetLeft();
		}
		if ((pCurrent == nullptr) && !St.empty())
			pCurrent = St.top();

	}
	else SetRetCode(Data::OUT_OF_RANGE);
	return GetRetCode();
}
```

### Базовый класс объектов-значений для сбалансированных деревьев (TBalanceNode).

#### Объявление:

```c++
#include "TTreeNode.h"

enum class Bal { BalOK, BalLeft, BalRight };

class  TBalanceNode : public TTreeNode {
protected:
	Bal Balance; // индекс балансировки вершины
public:
	TBalanceNode(TKey k = "", PTDatValue pVal = NULL, PTTreeNode pL = NULL,
		PTTreeNode pR = NULL, Bal bal = Bal::BalOK) : TTreeNode(k, pVal, pL, pR),
		Balance(bal) {};			// конструктор
	virtual TDatValue * GetCopy();  // изготовить копию
	Bal GetBalance(void) const;
	void SetBalance(Bal bal);
	friend class TBalanceTree;
};

typedef TBalanceNode *PTBalanceNode;
```

#### Определение:

```c++
#include "TBalanceNode.h"

TDatValue * TBalanceNode::GetCopy()
{
	TBalanceNode *tmp = new TBalanceNode(Key, pValue, nullptr, nullptr, Bal::BalOK);
	return tmp;
}

Bal TBalanceNode::GetBalance(void) const
{
	return Balance;
}

void TBalanceNode::SetBalance(Bal bal)
{
	Balance = bal;
}
```

### Класс  для сбалансированных деревьев (TBalanceTree).

#### Объявление:

```c++
#include "TTreeTable.h"
#include "TBalanceNode.h"

enum class Height { OK, Inc };

class  TBalanceTree : public TTreeTable {
protected:
	Height InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal);
	Height LeftTreeBalancing(PTBalanceNode &pNode);  // баланс. левого поддерева
	Height RightTreeBalancing(PTBalanceNode &pNode); // баланс. правого поддерева
public:
	TBalanceTree() :TTreeTable() {} // конструктор
	//основные методы
	virtual void InsRecord(TKey k, PTDatValue pVal) override final; // вставить
	virtual void DelRecord(TKey k) override final;                  // удалить
};

typedef TBalanceTree *PTBalanceTree;
```

#### Определение:

```c++
#include "TBalanceTree.h"

Height TBalanceTree::InsBalanceTree(PTBalanceNode &pNode, TKey k, PTDatValue pVal)
{
	Height HeightIndex = Height::OK;
	if (pNode == nullptr) { // вставка вершины
		pNode = new TBalanceNode(k, pVal);
		HeightIndex = Height::Inc;
		DataCount++;
	}
	else if (k < pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pLeft, k, pVal) == Height::Inc) {
			HeightIndex = LeftTreeBalancing(pNode);
		}
	}
	else if (k > pNode->GetKey()) {
		if (InsBalanceTree((PTBalanceNode&)pNode->pRight, k, pVal) == Height::Inc) {
			HeightIndex = RightTreeBalancing(pNode);
		}
	}
	else {
		SetRetCode(Data::DOUBLE_REC);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::LeftTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalRight:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalLeft);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalLeft:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetLeft());
		if (p1->GetBalance() == Bal::BalLeft) {
			pNode->pLeft = p1->pRight;
			p1->pRight = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetRight());
			p1->pRight = p2->pLeft;
			p2->pLeft = p1;
			pNode->pLeft = p2->pRight;
			p2->pRight = pNode;
			if (p2->GetBalance() == Bal::BalLeft) {
				pNode->SetBalance(Bal::BalRight);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalRight) {
				p1->SetBalance(Bal::BalLeft);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

Height TBalanceTree::RightTreeBalancing(PTBalanceNode &pNode)
{
	Height HeightIndex = Height::OK;
	switch (pNode->GetBalance()) {
	case Bal::BalLeft:
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
		break;
	case Bal::BalOK:
		pNode->SetBalance(Bal::BalRight);
		HeightIndex = Height::Inc;
		break;
	case Bal::BalRight:
		PTBalanceNode p1, p2;
		p1 = PTBalanceNode(pNode->GetRight());
		if (p1->GetBalance() == Bal::BalRight) {
			pNode->pRight = p1->pLeft;
			p1->pLeft = pNode;
			pNode->SetBalance(Bal::BalOK);
			pNode = p1;
		}
		else {
			p2 = PTBalanceNode(p1->GetLeft());
			p1->pLeft = p2->pRight;
			p2->pRight = p1;
			pNode->pRight = p2->pLeft;
			p2->pLeft = pNode;
			if (p2->GetBalance() == Bal::BalRight) {
				pNode->SetBalance(Bal::BalLeft);
			}
			else {
				pNode->SetBalance(Bal::BalOK);
			}
			if (p2->GetBalance() == Bal::BalLeft) {
				p1->SetBalance(Bal::BalRight);
			}
			else {
				p1->SetBalance(Bal::BalOK);
			}
			pNode = p2;
		}
		pNode->SetBalance(Bal::BalOK);
		HeightIndex = Height::OK;
	}
	return HeightIndex;
}

void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
	if (IsFull()) {
		SetRetCode(Data::FULL_TAB);
	}
	else {
		InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
	}
}

void TBalanceTree::DelRecord(TKey k)
{
	if (FindRecord(k) == nullptr) {
		SetRetCode(Data::NO_RECORD);
	}
	else {
		PTTreeNode tmp = pRoot;

		while (!St.empty())
			St.pop();
		while (tmp->GetKey() != k) {
			St.push(tmp);
			if (tmp->GetKey() < k)
				tmp = tmp->GetRight();
			else
				tmp = tmp->GetLeft();
		}

		TKey k2 = tmp->GetKey();
		// удаление листа
		if ((tmp->pLeft == nullptr) && (tmp->pRight == nullptr)) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = nullptr;
					if (prev->GetLeft() == tmp)
						prev->pLeft = nullptr;
				}
			}
			else {
				pRoot = nullptr;
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (справа)
		else if (tmp->pLeft == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с одним потомком (слева)
		else if (tmp->pRight == nullptr) {
			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pLeft;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pLeft;
				}
			}
			else {
				pRoot = tmp->GetLeft();
			}
			delete tmp;
			DataCount--;
		}
		// удаление звена с двумя потомками
		else {
			PTTreeNode down_left = tmp->GetRight();
			while (down_left->GetLeft() != nullptr)
				down_left = down_left->pLeft;
			down_left->pLeft = tmp->GetLeft();

			if (!St.empty()) {
				PTTreeNode prev = St.top();
				if (prev != nullptr) {
					if (prev->GetRight() == tmp)
						prev->pRight = tmp->pRight;
					if (prev->GetLeft() == tmp)
						prev->pLeft = tmp->pRight;
				}
			}
			else {
				pRoot = tmp->GetRight();
			}
			delete tmp;
			DataCount--;
		}
		if (pRoot != nullptr) {
			if (k2 < pRoot->GetKey()) {
				LeftTreeBalancing((PTBalanceNode&)pRoot);
			}
			else  if (k2 > pRoot->GetKey()) {
				RightTreeBalancing((PTBalanceNode&)pRoot);
			}
		}
	}
}
```

### Тесты

```c++
#include "gtest.h"
#include "TScanTable.h"
#include "TSortTable.h"
#include "TTreeTable.h"


/*---------------TTABRECORD---------------*/

TEST(TTabRecord_Test, can_get_key) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	EXPECT_EQ(expected.GetKey(), "test");
}

TEST(TTabRecord_Test, can_get_value) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	EXPECT_EQ(expected.GetValuePtr(), tmp);
}

TEST(TTabRecord_Test, can_set_value) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TDatValue* exp = new TTabRecord("????", NULL);
	TTabRecord expected("test", tmp);
	expected.SetValuePtr(exp);
	EXPECT_EQ(expected.GetValuePtr(), exp);
}

TEST(TTabRecord_Test, can_set_key) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	expected.SetKey("EXPECT");
	EXPECT_EQ(expected.GetKey(), "EXPECT");
}

TEST(TTabRecord_Test, can_assign_and_compare) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	TTabRecord res;
	res = expected;
	EXPECT_EQ(res, expected);
}

TEST(TTabRecord_Test, can_compare_inequality_1) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected_1("test", tmp);
	TTabRecord expected_2("tesx", tmp);

	EXPECT_TRUE(expected_1 < expected_2);
}

TEST(TTabRecord_Test, can_compare_inequality_2) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected_1("test", tmp);
	TTabRecord expected_2("tesx", tmp);

	EXPECT_FALSE(expected_1 > expected_2);
}

/*---------------ALLTABLES---------------*/

TEST(AllTables_Test, can_get_size) {
	TScanTable t(10);	

	EXPECT_EQ(t.GetTabSize(), 10);
}

TEST(AllTables_Test, can_get_sing_of_full) {
	TScanTable t(1);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);
	
	EXPECT_TRUE(t.IsFull());
}

TEST(AllTables_Test, can_get_sing_of_empty) {
	TScanTable t(1);	

	EXPECT_TRUE(t.IsEmpty());
}

TEST(AllTables_Test, can_get_sing_of_end) {
	TScanTable t(0);
	
	EXPECT_TRUE(t.IsTabEnded());
}

TEST(AllTables_Test, can_go_next) {
	TScanTable t(1);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);	
	t.GoNext();	

	EXPECT_TRUE(t.IsTabEnded());
}

/*---------------TSCANTABLES---------------*/

TEST(TScanTable_Test, cant_find_in_empty_table) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.FindRecord("test");

	EXPECT_EQ(t.GetRetCode(), Data::NO_RECORD);
}


TEST(TScanTable_Test, can_insert_and_find_in_not_empty) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);	

	EXPECT_EQ(t.FindRecord("test"), tmp);
}

TEST(TScanTable_Test, cant_del_in_empty_table) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.DelRecord("test");

	EXPECT_EQ(t.GetRetCode(), Data::NO_RECORD);
}

TEST(TScanTable_Test, can_del_in_not_empty_table) {
	TScanTable t(10);	
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);
	t.DelRecord("test");
	t.FindRecord("test");

	EXPECT_EQ(t.GetRetCode(), Data::NO_RECORD);
}

/*---------------TSORTTABLES---------------*/

TEST(TSortTable_Test, can_create_from_scan_table) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);	
	t.InsRecord("test", tmp);
	TSortTable p(10);
	p = t;
	TTabRecord* exp = p.GetCurrRecord();

	EXPECT_EQ(exp->GetKey(), t.GetKey());
}

TEST(TSortTable_Test, table_sort_after_insert) {
	TSortTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);
	t.InsRecord("aaaa", tmp);
	t.Reset();

	EXPECT_EQ(t.GetKey(), "aaaa");
}

/*---------------TTREETABLES---------------*/

TEST(TTreeTable_Test, CanGetRecord) {
	TTreeTable t;
	t.InsRecord("a", 0);
	EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TTreeTable_Test, CanGetKey) {
	TTreeTable t;
	t.InsRecord("a", NULL);
	t.Reset();
	EXPECT_EQ(t.GetKey(), "a");
}

TEST(TTreeTable_Test, CanCheckEnded) {
	TTreeTable t;
	t.Reset();
	EXPECT_TRUE(t.IsTabEnded());
}
```

![Tests](https://bytebucket.org/Marat_Israfilov/mp2-lab7-tables/raw/c8550c669c097088c784604d9ff11ac66839f1eb/src/DmitrichevIsrafilov/tests/Tests.png)

## Вывод:

В данной лабораторной работе была разработана иерархия классов:

* TTabRecord - класс записи таблицы.
* TTable - базовый класс табица.
* TArrayTable - класс для просмотровых и сортированных таблиц(наследуется от TTable).
* TTreeNode, TBalanceNode - класс элемент дерева.

На представленных выше классах были изучены и созданы __Таблицы__ нескольких видов:

* Просмотровые таблицы - TScanTable (самый простой тип).
* Сортированные таблицы - TSortTable.
* Таблица на деревьях - TTreeTable, TBalanceTree.
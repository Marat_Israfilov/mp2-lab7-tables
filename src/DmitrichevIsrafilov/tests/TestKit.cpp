#include "gtest.h"
#include "TScanTable.h"
#include "TSortTable.h"
#include "TTreeTable.h"


/*---------------TTABRECORD---------------*/

TEST(TTabRecord_Test, can_get_key) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	EXPECT_EQ(expected.GetKey(), "test");
}

TEST(TTabRecord_Test, can_get_value) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	EXPECT_EQ(expected.GetValuePtr(), tmp);
}

TEST(TTabRecord_Test, can_set_value) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TDatValue* exp = new TTabRecord("????", NULL);
	TTabRecord expected("test", tmp);
	expected.SetValuePtr(exp);
	EXPECT_EQ(expected.GetValuePtr(), exp);
}

TEST(TTabRecord_Test, can_set_key) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	expected.SetKey("EXPECT");
	EXPECT_EQ(expected.GetKey(), "EXPECT");
}

TEST(TTabRecord_Test, can_assign_and_compare) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected("test", tmp);
	TTabRecord res;
	res = expected;
	EXPECT_EQ(res, expected);
}

TEST(TTabRecord_Test, can_compare_inequality_1) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected_1("test", tmp);
	TTabRecord expected_2("tesx", tmp);

	EXPECT_TRUE(expected_1 < expected_2);
}

TEST(TTabRecord_Test, can_compare_inequality_2) {
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	TTabRecord expected_1("test", tmp);
	TTabRecord expected_2("tesx", tmp);

	EXPECT_FALSE(expected_1 > expected_2);
}

/*---------------ALLTABLES---------------*/

TEST(AllTables_Test, can_get_size) {
	TScanTable t(10);	

	EXPECT_EQ(t.GetTabSize(), 10);
}

TEST(AllTables_Test, can_get_sing_of_full) {
	TScanTable t(1);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);
	
	EXPECT_TRUE(t.IsFull());
}

TEST(AllTables_Test, can_get_sing_of_empty) {
	TScanTable t(1);	

	EXPECT_TRUE(t.IsEmpty());
}

TEST(AllTables_Test, can_get_sing_of_end) {
	TScanTable t(0);
	
	EXPECT_TRUE(t.IsTabEnded());
}

TEST(AllTables_Test, can_go_next) {
	TScanTable t(1);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);	
	t.GoNext();	

	EXPECT_TRUE(t.IsTabEnded());
}

/*---------------TSCANTABLES---------------*/

TEST(TScanTable_Test, cant_find_in_empty_table) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.FindRecord("test");

	EXPECT_EQ(t.GetRetCode(), Data::NO_RECORD);
}


TEST(TScanTable_Test, can_insert_and_find_in_not_empty) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);	

	EXPECT_EQ(t.FindRecord("test"), tmp);
}

TEST(TScanTable_Test, cant_del_in_empty_table) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.DelRecord("test");

	EXPECT_EQ(t.GetRetCode(), Data::NO_RECORD);
}

TEST(TScanTable_Test, can_del_in_not_empty_table) {
	TScanTable t(10);	
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);
	t.DelRecord("test");
	t.FindRecord("test");

	EXPECT_EQ(t.GetRetCode(), Data::NO_RECORD);
}

/*---------------TSORTTABLES---------------*/

TEST(TSortTable_Test, can_create_from_scan_table) {
	TScanTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);	
	t.InsRecord("test", tmp);
	TSortTable p(10);
	p = t;
	TTabRecord* exp = p.GetCurrRecord();

	EXPECT_EQ(exp->GetKey(), t.GetKey());
}

TEST(TSortTable_Test, table_sort_after_insert) {
	TSortTable t(10);
	TDatValue* tmp = new TTabRecord("xxxx", NULL);
	t.InsRecord("test", tmp);
	t.InsRecord("aaaa", tmp);
	t.Reset();

	EXPECT_EQ(t.GetKey(), "aaaa");
}

/*---------------TTREETABLES---------------*/

TEST(TTreeTable_Test, CanGetRecord) {
	TTreeTable t;
	t.InsRecord("a", 0);
	EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TTreeTable_Test, CanGetKey) {
	TTreeTable t;
	t.InsRecord("a", NULL);
	t.Reset();
	EXPECT_EQ(t.GetKey(), "a");
}

TEST(TTreeTable_Test, CanCheckEnded) {
	TTreeTable t;
	t.Reset();
	EXPECT_TRUE(t.IsTabEnded());
}
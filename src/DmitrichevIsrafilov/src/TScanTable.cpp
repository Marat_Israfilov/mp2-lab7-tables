#include "TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
	SetRetCode(Data::OK);

	for (int i = 0; i < TabSize; ++i) {
		if(pRecs[i] != nullptr) 
			if (pRecs[i]->GetKey() == k) {
				return pRecs[i]->GetValuePtr();
			}

	}
	SetRetCode(Data::NO_RECORD);
	return nullptr;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
	SetRetCode(Data::OUT_OF_RANGE);
	for (int i = 0; i < TabSize; ++i) {
		if (pRecs[i] == nullptr)
		{
			pRecs[i] = new TTabRecord(k, pVal);
			DataCount++;
			SetRetCode(Data::OK);
			break;
		}
	}
}

void TScanTable::DelRecord(TKey k)
{
	SetRetCode(Data::NO_RECORD);
	for (int i = 0; i < TabSize; i++) {
		if (pRecs[i] != nullptr)
			if (pRecs[i]->GetKey() == k) {
				delete pRecs[i];
				pRecs[i] = pRecs[DataCount - 1];
				pRecs[DataCount - 1] = nullptr;
				DataCount--;
				SetRetCode(Data::OK);
			}
	}
}